/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2021 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <sys/param.h>
#include <sys/module.h>
#include <sys/nv.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/sysctl.h>

#include "sysctl_handle_nvlist.h"

static struct sysctl_ctx_list clist;
static struct sysctl_oid *root;
static nvlist_t *nvl;
static const char *cities[5] = {"Avellino", "Benevento", "Caserta", "Napoli", "Salerno"};

static int
sysctlcampania_modevent(module_t mod __unused, int event, void *arg __unused)
{
    int error = 0;
    
    switch (event) {
    case MOD_LOAD:
	sysctl_ctx_init(&clist);
	root = SYSCTL_ADD_ROOT_NODE(NULL, OID_AUTO, "nv", CTLFLAG_RW, 0, "nv root");

	nvl = nvlist_create(0);
	nvlist_add_string(nvl, "Capital", "Napoli");
	nvlist_add_number(nvl, "Population", 5869029);
	nvlist_add_string_array(nvl, "Cities", cities, 5);

	SYSCTL_ADD_NVLIST(NULL, SYSCTL_CHILDREN(root), OID_AUTO, "campania",
	    CTLFLAG_RD, nvl, "Administrative region of Italy");
	uprintf("sysctlcampania kernel module loaded.\n");
	break;
    case MOD_UNLOAD:
	sysctl_remove_oid(root,1,1);
	if (sysctl_ctx_free(&clist)) {
	    uprintf("sysctl_ctx_free failed.\n");
	    return (ENOTEMPTY);
	}
	nvlist_destroy(nvl);
	uprintf("sysctlcampania kernel module unloaded.\n");
	break;
    default:
	error = EOPNOTSUPP;
	break;
    }
    return (error);
}
static moduledata_t sysctlcampania_mod = {
    "sysctlcampania",
    sysctlcampania_modevent,
    NULL
};

DECLARE_MODULE(sysctlcampania, sysctlcampania_mod, SI_SUB_EXEC, SI_ORDER_ANY);
