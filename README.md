# sysctl-libnv

[FreeBSD](https://www.freebsd.org/) provides
[libnv(9)](https://man.freebsd.org/libnv/9) for _name/value pairs_,
this project represents the effort to add _libnv_ to
[sysctl](https://docs.freebsd.org/en/books/handbook/config/#configtuning-sysctl),
[BSDCan Question](https://github.com/BSDCan/2020-Q-A/blob/master/Alfonso_S._Siciliano_sysctlinfo_a_new_interface_to_visit_the_FreeBSD_sysctl_MIB_and_to_pass_the_objects).

Install [sysutils/nsysctl](https://www.freshports.org/sysutils/nsysctl) >= 2.0,
then:
```
% git clone https://gitlab.com/alfix/sysctl-libnv.git
% cd sysctl-libnv
% make ; sudo make load ; make clean
% nsysctl nv.campania
nv.campania: 
Capital=Napoli
Population=5869029
Cities=Avellino Benevento Caserta Napoli Salerno
% nsysctl --libxo=xml,pretty -dt nv.campania
<object>
  <name>nv.campania</name>
  <description>Administrative region of Italy</description>
  <type>opaque</type>
  <value>
    <nvlist>
      <nv>
        <name>Capital</name>
        <value>Napoli</value>
        <nvtype>NV_TYPE_STRING</nvtype>
      </nv>
      <nv>
        <name>Population</name>
        <value>5869029</value>
        <nvtype>NV_TYPE_NUMBER</nvtype>
      </nv>
      <nv>
        <name>Cities</name>
        <value>Avellino</value>
        <value>Benevento</value>
        <value>Caserta</value>
        <value>Napoli</value>
        <value>Salerno</value>
        <nvtype>NV_TYPE_STRING_ARRAY</nvtype>
      </nv>
    </nvlist>
  </value>
</object>
```
