Testing nv types:
```
% make ; sudo make load ; make clean
% nsysctl nv_test.nvlist_test
nv_test.nvlist_test: 
Name1-Null
Name2-Bool=true
Name3-Bool=false
Name4-String=Value4
Name5-Number=5
Name6-Binary=313200
 List2-First=L2-Value1
 List2-Second=L2-Value2
Name8-BoolArray=true false true
Name9-NumberArray=100 200 300
Name10-StringArray=FreeBSD sysctl libnv
List-Array-0=LA0-Value
List-Array-1=LA1-Value
List-Array-2=LA2-Value
% nsysctl -t nv_test.nvlist_test
nv_test.nvlist_test: opaque: 
Name1-Null NV_TYPE_NULL
Name2-Bool=true NV_TYPE_BOOL
Name3-Bool=false NV_TYPE_BOOL
Name4-String=Value4 NV_TYPE_STRING
Name5-Number=5 NV_TYPE_NUMBER
Name6-Binary=313200 NV_TYPE_BINARY
 List2-First=L2-Value1 NV_TYPE_STRING
 List2-Second=L2-Value2 NV_TYPE_STRING
Name8-BoolArray=true false true NV_TYPE_BOOL_ARRAY
Name9-NumberArray=100 200 300 NV_TYPE_NUMBER_ARRAY
Name10-StringArray=FreeBSD sysctl libnv NV_TYPE_STRING_ARRAY
List-Array-0=LA0-Value NV_TYPE_STRING
List-Array-1=LA1-Value NV_TYPE_STRING
List-Array-2=LA2-Value NV_TYPE_STRING
% nsysctl --libxo=xml,pretty -t nv_test.nvlist_test
<object>
  <name>nv_test.nvlist_test</name>
  <type>opaque</type>
  <value>
    <nvlist>
      <nv>
        <name>Name1-Null</name>
        <nvtype>NV_TYPE_NULL</nvtype>
      </nv>
      <nv>
        <name>Name2-Bool</name>
        <value>true</value>
        <nvtype>NV_TYPE_BOOL</nvtype>
      </nv>
      <nv>
        <name>Name3-Bool</name>
        <value>false</value>
        <nvtype>NV_TYPE_BOOL</nvtype>
      </nv>
      <nv>
        <name>Name4-String</name>
        <value>Value4</value>
        <nvtype>NV_TYPE_STRING</nvtype>
      </nv>
      <nv>
        <name>Name5-Number</name>
        <value>5</value>
        <nvtype>NV_TYPE_NUMBER</nvtype>
      </nv>
      <nv>
        <name>Name6-Binary</name>
        <value>31</value>
        <value>32</value>
        <value>0</value>
        <nvtype>NV_TYPE_BINARY</nvtype>
      </nv>
      <nvlist>
        <nv>
          <name>List2-First</name>
          <value>L2-Value1</value>
          <nvtype>NV_TYPE_STRING</nvtype>
        </nv>
        <nv>
          <name>List2-Second</name>
          <value>L2-Value2</value>
          <nvtype>NV_TYPE_STRING</nvtype>
        </nv>
      </nvlist>
      <nv>
        <name>Name8-BoolArray</name>
        <value>true</value>
        <value>false</value>
        <value>true</value>
        <nvtype>NV_TYPE_BOOL_ARRAY</nvtype>
      </nv>
      <nv>
        <name>Name9-NumberArray</name>
        <value>100</value>
        <value>200</value>
        <value>300</value>
        <nvtype>NV_TYPE_NUMBER_ARRAY</nvtype>
      </nv>
      <nv>
        <name>Name10-StringArray</name>
        <value>FreeBSD</value>
        <value>sysctl</value>
        <value>libnv</value>
        <nvtype>NV_TYPE_STRING_ARRAY</nvtype>
      </nv>
      <nv>
        <name>List-Array-0</name>
        <value>LA0-Value</value>
        <nvtype>NV_TYPE_STRING</nvtype>
      </nv>
      <nv>
        <name>List-Array-1</name>
        <value>LA1-Value</value>
        <nvtype>NV_TYPE_STRING</nvtype>
      </nv>
      <nv>
        <name>List-Array-2</name>
        <value>LA2-Value</value>
        <nvtype>NV_TYPE_STRING</nvtype>
      </nv>
    </nvlist>
  </value>
</object>
```
