/*-
 * SPDX-License-Identifier: CC0-1.0
 *
 * Written in 2021 by Alfonso Sabato Siciliano.
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty, see:
 *   <http://creativecommons.org/publicdomain/zero/1.0/>.
 */
 
#include <sys/param.h>
#include <sys/module.h>
#include <sys/nv.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/sysctl.h>

#include "../sysctl_handle_nvlist.h"

static struct sysctl_ctx_list clist;
static struct sysctl_oid *root;
static nvlist_t *nvl;
static unsigned char binary[3];
static const bool boolarray[3] = {true, false, true};
static const uint64_t numberarray[3] = {100, 200, 300};
static const char *stringarray[3] = {"FreeBSD", "sysctl", "libnv"};
static nvlist_t *nvlinner;

static int
addnvobjects_modevent(module_t mod __unused, int event, void *arg __unused)
{
    int error = 0;
    
    switch (event) {
    case MOD_LOAD:
	sysctl_ctx_init(&clist);
	root = SYSCTL_ADD_ROOT_NODE(NULL, OID_AUTO, "nv_test", CTLFLAG_RD,
	    0, "nv root");

	nvl = nvlist_create(0);
	nvlist_add_null(nvl, "Name1-Null");
	nvlist_add_bool(nvl, "Name2-Bool", true);
	nvlist_add_bool(nvl, "Name3-Bool", false);
	nvlist_add_string(nvl, "Name4-String", "Value4");
	nvlist_add_number(nvl, "Name5-Number", 5);
	strcpy(binary, "12");
	nvlist_add_binary(nvl, "Name6-Binary", &binary, 3);
	/* ifndef _KERNEL
	 * nvlist_add_descriptor(nvl, "Name7-Description", 7);
	 * nvlist_add_descriptor_array() */

	nvlinner = nvlist_create(0);
	nvlist_add_string(nvlinner, "List2-First",  "L2-Value1");
	nvlist_add_string(nvlinner, "List2-Second", "L2-Value2");
	nvlist_add_nvlist(nvl, "List2", nvlinner);

	nvlist_add_bool_array(nvl, "Name8-BoolArray", boolarray, 3);
	nvlist_add_number_array(nvl, "Name9-NumberArray", numberarray, 3);
	nvlist_add_string_array(nvl, "Name10-StringArray", stringarray, 3);


	nvlist_t *a0 = nvlist_create(0);
	nvlist_add_string(a0, "List-Array-0", "LA0-Value");
	nvlist_t *a1 = nvlist_create(0);
	nvlist_add_string(a1, "List-Array-1", "LA1-Value");
	nvlist_t *a2 = nvlist_create(0);
	nvlist_add_string(a2, "List-Array-2", "LA2-Value");

	const nvlist_t *listarray[3] = {a0, a1, a2};
	nvlist_add_nvlist_array(nvl, "nvArray", listarray, 3);

	SYSCTL_ADD_NVLIST(NULL, SYSCTL_CHILDREN(root), OID_AUTO, "nvlist_test",
	    CTLFLAG_RD, nvl, "Object libnv");
	uprintf("addnvobjects kernel module loaded.\n");
	break;
    case MOD_UNLOAD:
	sysctl_remove_oid(root,1,1);
	if (sysctl_ctx_free(&clist)) {
	    uprintf("sysctl_ctx_free failed.\n");
	    return (ENOTEMPTY);
	}
	nvlist_destroy(nvl);
	uprintf("addnvobjects kernel module unloaded.\n");
	break;
    default:
	error = EOPNOTSUPP;
	break;
    }
    return (error);
}
static moduledata_t addnvobjects_mod = {
	"addnvobjects",
	addnvobjects_modevent,
	NULL
};

DECLARE_MODULE(addnvobjects, addnvobjects_mod, SI_SUB_EXEC, SI_ORDER_ANY);
